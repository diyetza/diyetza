<?php

class References_model extends CI_Model
{

    public $tableName = "references";

    public function __construct()
    {
        parent::__construct();

    }
    /**1 kayıdı  bana getirecek method **/
    public function get($where = array())
    {
        return $this -> db -> where($where) -> get($this->tableName) -> row();
    }
    /** Tüm Kaydı result eden method */
    public function get_all($where = array())
    {
        return $this -> db  -> where($where) -> get($this->tableName) -> result();
    }
    /** Ürün Ekleme Methodu */
    public function add($data = array())
    {
        return $this -> db -> insert($this->tableName, $data);
    }
    /** Update fonksiyonu */
    public function update($where = array(), $data = array())
    {
        return $this -> db -> where($where) -> update($this->tableName, $data);
    }
    /** Silme Fonksiyonu */
    public function delete($where = array())
    {
        return $this->db->where($where)->delete($this->tableName);
    }




}