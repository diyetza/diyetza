<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public $viewFolder = "";

    public function __construct()
    {
        parent::__construct();
        $this -> viewFolder = "dashboard_v";
        $this -> load -> model("dashboard_model");

    }

    public function index()
	{
        $viewData = new stdClass();
        $items = $this -> dashboard_model -> get_all();


        $viewData -> viewFolder = $this->viewFolder;
        $viewData -> subViewFolder = "";
        $viewData -> items = $items;

        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
	}

	public function todo_save()
    {
        $insert = $this -> dashboard_model -> add(
            array(
                "description" => $this -> input -> post("description"),
                "isActive" => 0,
                "createdAt" => date("Y-m-d H:i:s")

            )
        );
        if ($insert)
        {
            redirect(base_url("dashboard"));
        }
        else
        {
            redirect(base_url("dashboard"));
        }
    }

    public function isActiveSetter($id)
    {

        if($id){

            $isActive = ($this->input->post("data") === "true") ? 1 : 0;

            $this->dashboard_model->update(
                array(
                    "id"    => $id
                ),
                array(
                    "isActive"  => $isActive
                )
            );
        }
    }

    public function delete($id){

        $delete = $this->dashboard_model->delete(
            array(
                "id"    => $id
            )
        );

        // TODO Alert Sistemi Eklenecek...
        if($delete){
            redirect(base_url("dashboard"));
        } else {
            redirect(base_url("dashboard"));
        }

    }





}
