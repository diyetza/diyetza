<div class="sidebar-container">
    <div class="sidemenu-container navbar-collapse collapse fixed-menu">
        <div id="remove-scroll" class="left-sidemenu">
            <ul class="sidemenu page-header-fixed sidemenu-hover-submenu" data-keep-expanded="false" data-auto-scroll="true"
                data-slide-speed="200" style="padding-top: 20px">
                <li class="sidebar-toggler-wrapper hide">
                    <div class="sidebar-toggler">
                        <span></span>
                    </div>
                </li>
                <li class="sidebar-user-panel">
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo base_url("assets"); ?>/img/diyetisyen.jpg" class="img-circle user-img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p> Dyt.Ezgi Çınar</p>
                            <a href="#"><i class="fa fa-circle user-online"></i><span class="txtOnline"> Online</span></a>
                        </div>
                    </div>
                </li>
                <li class="nav-item ">
                    <a href="<?php echo base_url(); ?>" class="nav-link nav-toggle">
                        <i class="fa fa-home"></i>
                        <span class="title">Anasayfa</span>
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?php echo base_url("product"); ?>">
                        <i class="material-icons">shopping_basket</i>
                        <span class="title">Ürünler</span><span class="arrow"></span></a>
                        <ul class="sub-menu">
                        <li class="nav-item  ">
                            <a href="<?php echo base_url("product/new_form"); ?>" class="nav-link "> <span class="title">Ürün Ekleme</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="<?php echo base_url("product"); ?>" class="nav-link "> <span class="title">Ürünleri İncele</span>
                            </a>
                        </li>

                    </ul>
                        <span class="arrow"></span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="<?php echo base_url("news"); ?>" class="nav-link nav-toggle"><i class="material-icons">announcement</i>
                        <span class="title">Haberler</span><span class="arrow"></span></a>
                    <ul class="sub-menu">
                        <li class="nav-item  ">
                            <a href="<?php echo base_url("news/new_form"); ?>" class="nav-link "> <span class="title">Haber Oluştur</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="<?php echo base_url("news"); ?>" class="nav-link "> <span class="title">Haberleri İncele</span>
                            </a>
                        </li>

                    </ul>

                </li>
                <li class="nav-item  ">
                    <a href="<?php echo base_url("references"); ?>" class="nav-link nav-toggle"> <i class="material-icons">link</i>
                        <span class="title">Referans</span> <span class="arrow"></span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="<?php echo base_url("calendar"); ?>" class="nav-link nav-toggle"> <i class="material-icons">alarm</i>
                        <span class="title">Takvim</span> <span class="arrow"></span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="<?php echo base_url("users"); ?>" class="nav-link nav-toggle"> <i class="material-icons">link</i>
                        <span class="title">Danışanlarım</span> <span class="arrow"></span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="#" class="nav-link nav-toggle"> <i class="material-icons">hotel</i>
                        <span class="title">Room Allotment</span> <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item  ">
                            <a href="room_allotment.html" class="nav-link "> <span class="title">Alloted Rooms</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="add_room_allotment.html" class="nav-link "> <span class="title">New Allotment</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="add_room_allotment_material.html" class="nav-link "> <span class="title">New Allotment Material</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="edit_room_allotment.html" class="nav-link "> <span class="title">Edit Allotment</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item  ">
                    <a href="#" class="nav-link nav-toggle"> <i class="material-icons">monetization_on</i>
                        <span class="title">Payments</span> <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item  ">
                            <a href="payments.html" class="nav-link "> <span class="title">Payments</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="add_payment.html" class="nav-link "> <span class="title">Add Payments</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="invoice_payment.html" class="nav-link "> <span class="title">Patient Invoice</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item  ">
                    <a href="widget.html" class="nav-link nav-toggle"> <i class="material-icons">widgets</i>
                        <span class="title">Widget</span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="#" class="nav-link nav-toggle">
                        <i class="material-icons">dvr</i>
                        <span class="title">UI Elements</span>
                        <span class="label label-rouded label-menu">10</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item  ">
                            <a href="ui_buttons.html" class="nav-link ">
                                <span class="title">Buttons</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="ui_tabs_accordions_navs.html" class="nav-link ">
                                <span class="title">Tabs &amp; Accordions</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="sweet_alert.html" class="nav-link ">
                                <span class="title">Alert</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="ui_typography.html" class="nav-link ">
                                <span class="title">Typography</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="ui_icons.html" class="nav-link ">
                                <span class="title">Icons</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="ui_modal.html" class="nav-link ">
                                <span class="title">Modals</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="ui_panels.html" class="nav-link ">
                                <span class="title">Panels</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="ui_grid.html" class="nav-link ">
                                <span class="title">Grids</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="calendar.html" class="nav-link ">
                                <span class="title">Calender</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="ui_tree.html" class="nav-link ">
                                <span class="title">Tree View</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="ui_carousel.html" class="nav-link ">
                                <span class="title">Carousel</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item  ">
                    <a href="#" class="nav-link nav-toggle">
                        <i class="material-icons">store</i>
                        <span class="title">Material Elements</span>
                        <span class="label label-rouded label-menu deepPink-bgcolor">14</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item  ">
                            <a href="material_button.html" class="nav-link ">
                                <span class="title">Buttons</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="material_tab.html" class="nav-link ">
                                <span class="title">Tabs</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="material_chips.html" class="nav-link ">
                                <span class="title">Chips</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="material_grid.html" class="nav-link ">
                                <span class="title">Grid</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="material_icons.html" class="nav-link ">
                                <span class="title">Icon</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="material_form.html" class="nav-link ">
                                <span class="title">Form</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="material_datepicker.html" class="nav-link ">
                                <span class="title">DatePicker</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="material_select.html" class="nav-link ">
                                <span class="title">Select Item</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="material_loading.html" class="nav-link ">
                                <span class="title">Loading</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="material_menu.html" class="nav-link ">
                                <span class="title">Menu</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="material_slider.html" class="nav-link ">
                                <span class="title">Slider</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="material_tables.html" class="nav-link ">
                                <span class="title">Tables</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="material_toggle.html" class="nav-link ">
                                <span class="title">Toggle</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="material_badges.html" class="nav-link ">
                                <span class="title">Badges</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item  ">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="material-icons">subtitles</i>
                        <span class="title">Forms </span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item  ">
                            <a href="layouts_form.html" class="nav-link ">
                                <span class="title">Form Layout</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="advance_form.html" class="nav-link ">
                                <span class="title">Advance Component</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="wizard_form.html" class="nav-link ">
                                <span class="title">Form Wizard</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="validation_form.html" class="nav-link ">
                                <span class="title">Form Validation</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="editable_form.html" class="nav-link ">
                                <span class="title">Editor</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item  ">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="material-icons">list</i>
                        <span class="title">Data Tables</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item  ">
                            <a href="basic_table.html" class="nav-link ">
                                <span class="title">Basic Tables</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="advanced_table.html" class="nav-link ">
                                <span class="title">Advance Tables</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="export_table.html" class="nav-link ">
                                <span class="title">Export Tables</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="child_row_table.html" class="nav-link ">
                                <span class="title">Child Row Tables</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="group_table.html" class="nav-link ">
                                <span class="title">Grouping</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="tableData.html" class="nav-link ">
                                <span class="title">Tables With Sourced Data</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item  ">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="material-icons">timeline</i>
                        <span class="title">Charts</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item  ">
                            <a href="charts_echarts.html" class="nav-link ">
                                <span class="title">eCharts</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="charts_morris.html" class="nav-link ">
                                <span class="title">Morris Charts</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="charts_chartjs.html" class="nav-link ">
                                <span class="title">Chartjs</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item  ">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="material-icons">map</i>
                        <span class="title">Maps</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item  ">
                            <a href="google_maps.html" class="nav-link ">
                                <span class="title">Google Maps</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="vector_maps.html" class="nav-link ">
                                <span class="title">Vector Maps</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item  ">
                    <a href="javascript:;" class="nav-link nav-toggle"> <i class="material-icons">description</i>
                        <span class="title">Extra pages</span>
                        <span class="label label-rouded label-menu purple-bgcolor">7</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item  ">
                            <a href="login.html" class="nav-link "> <span class="title">Login</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="sign_up.html" class="nav-link "> <span class="title">Sign Up</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="forgot_password.html" class="nav-link "> <span class="title">Forgot Password</span>
                            </a>
                        </li>
                        <li class="nav-item  "><a href="user_profile.html" class="nav-link "><span class="title">Profile</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="contact.html" class="nav-link "> <span class="title">Contact Us</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="lock_screen.html" class="nav-link "> <span class="title">Lock Screen</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="page-404.html" class="nav-link "> <span class="title">404 Page</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="page-500.html" class="nav-link "> <span class="title">500 Page</span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="blank_page.html" class="nav-link "> <span class="title">Blank Page</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="material-icons">slideshow</i>
                        <span class="title">Multi Level Menu</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-university"></i> Item 1
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item">
                                    <a href="javascript:;" class="nav-link nav-toggle">
                                        <i class="fa fa-bell-o"></i> Arrow Toggle
                                        <span class="arrow "></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item">
                                            <a href="javascript:;" class="nav-link">
                                                <i class="fa fa-calculator"></i> Sample Link 1</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#" class="nav-link">
                                                <i class="fa fa-clone"></i> Sample Link 2</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#" class="nav-link">
                                                <i class="fa fa-cogs"></i> Sample Link 3</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        <i class="fa fa-file-pdf-o"></i> Sample Link 1</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        <i class="fa fa-rss"></i> Sample Link 2</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        <i class="fa fa-hdd-o"></i> Sample Link 3</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-gavel"></i> Arrow Toggle
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        <i class="fa fa-paper-plane"></i> Sample Link 1</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        <i class="fa fa-power-off"></i> Sample Link 1</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        <i class="fa fa-recycle"></i> Sample Link 1
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="fa fa-volume-up"></i> Item 3 </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
