<script src="<?php echo base_url("assets"); ?>/assets/jquery.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/assets/popper/popper.js"></script>
<script src="<?php echo base_url("assets"); ?>/assets/jquery.blockui.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/assets/jquery.slimscroll.js"></script>
<script src="<?php echo base_url("assets"); ?>/assets/jquery-ui/jquery-ui.min.js"></script>
<!-- bootstrap -->
<script src="<?php echo base_url("assets"); ?>/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/assets/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- counterup -->
<script src="<?php echo base_url("assets"); ?>/assets/counterup/jquery.waypoints.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/assets/counterup/jquery.counterup.min.js"></script>
<!-- Common js-->
<script src="<?php echo base_url("assets"); ?>/assets/app.js"></script>
<script src="<?php echo base_url("assets"); ?>/assets/layout.js"></script>
<script src="<?php echo base_url("assets"); ?>/assets/theme-color.js"></script>
<!-- material -->
<script src="<?php echo base_url("assets"); ?>/assets/material/material.min.js"></script>
<!-- chart js -->
<!-- calendar js -->
<script src="<?php echo base_url("assets"); ?>/assets/moment.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/assets/fullcalendar/fullcalendar.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/assets/calendar.min.js"></script>

<script src="<?php echo base_url("assets"); ?>/assets/custom.js"></script>

<script src="<?php echo base_url("assets"); ?>/assets/chart-js/Chart.bundle.js"></script>
<script src="<?php echo base_url("assets"); ?>/assets/chart-js/utils.js"></script>
<script src="<?php echo base_url("assets"); ?>/assets/chart-js/home-data.js"></script>
<script src="<?php echo base_url("assets"); ?>/assets/sweet-alert/sweetalert.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/assets/sweet-alert/sweet-alert-data.js"></script>
<script src="<?php echo base_url("assets"); ?>/assets/sweetalert2.all.js"></script>
<script src="<?php echo base_url("assets"); ?>/assets/sweetalert2.min.js"></script>

<script src="<?php echo base_url("assets"); ?>/assets/dropzone/dropzone.js"></script>
<script src="<?php echo base_url("assets"); ?>/assets/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/assets/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/assets/table_data.js"></script>
<script src="<?php echo base_url("assets"); ?>/assets/profile.js"></script>



<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

