<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Danışan Yönetim Paneli</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Anasayfa</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Yönetim Paneli</li>
                </ol>
            </div>
        </div>
        <!-- start widget -->
        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="card  card-topline-aqua">
                    <div class="card-head">
                        <header>YAPILACAKLAR LİSTESİ</header>
                        <div class="tools">
                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="card-body no-padding height-9">
                        <div class="row">
                            <ul class="to-do-list ui-sortable" id="isActive">
                                <?php foreach ($items as $item) { ?>
                                <li class="clearfix">
                                    <div class="todo-check pull-left">
                                        <input
                                                class="isActive" type="checkbox" <?php echo ($item ->isActive) ? "checked": ""; ?>>
                                        <label for="todo-check label" data-url="<?php echo base_url("dashboard/isActiveSetter/{$item ->id}"); ?>"></label>
                                    </div>
                                    <p class="todo-title"><?php echo $item ->description; ?>
                                    </p>
                                    <div class="todo-actionlist pull-right clearfix">
                                        <a href="<?php echo base_url("dashboard/delete/{$item ->id}"); ?>"class="remove-btn"><i class="fa fa-times"></i></a>
                                    </div>
                                </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="box-footer">
                            <form action="<?php echo base_url("dashboard/todo_save"); ?>" method="post">
                                <div class="input-group">
                                    <input type="text" name="description" placeholder="Yapılacaklar Listenize Birşeyler Ekleyin..." class="form-control">

													<button type="submit" class="btn btn-warning btn-flat">Ekle</button>
                                     </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        </div>

        <!-- end widget -->
        <!-- chart start -->
        <!-- Chart end -->
        <!-- start admited patient list -->
        <!-- end admited patient list -->
    </div>
</div>
