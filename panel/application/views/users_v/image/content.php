<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Danışan Sağlık Raporları</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Anasayfa</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Danışan Sağlık Raporları</li>
                </ol>
            </div>
        </div>
        <!--Dropzone ile danışana rapor yükleme -->
        <div class="col-md-12">
            <div class="card card-topline-purple">
                <div class="card-head">
                    <header><?php echo $item->kullanici_isim; ?> <?php echo $item->kullanici_soyisim; ?> DANIŞANINA RAPOR YÜKLEME</header>
                    <div class="tools">
                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                        <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                        <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                    </div>
                </div>
                <div class="card-body" id="bar-parent">
                    <form data-url="<?php echo base_url("users/refresh_image_list/{$item->id}"); ?>" id="dropzone" class="dropzone" action="<?php echo base_url("users/image_upload/{$item->id}"); ?>"   data-options="{ url: '<?php echo base_url("users/image_upload/{$item->id}"); ?>'}"></form>
                </div>
            </div>
            <div class="row">

            </div>

        </div>
        <!--Danışana Ait raporları listeleme,verileri tablo halinde görüntüleme -->
        <div class="col-md-12">
            <div class="card card-topline-aqua">
                <div class="card-head">
                    <header><?php echo $item->kullanici_isim; ?> <?php echo $item->kullanici_soyisim; ?> DANIŞANINA AİT RAPORLAR</header>
                    <div class="tools">
                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                        <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                        <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                    </div>
                </div>
                <div class="card-body" id="bar-parent">
                        <?php $this->load->view("{$viewFolder}/{$subViewFolder}/render_elements/image_list_v"); ?>
                </div>
                    </div><!-- .widget-body -->




            <!-- start widget -->
        <!-- end widget -->
        <!-- chart start -->
        <!-- Chart end -->
        <!-- start admited patient list -->
        <!-- end admited patient list -->
    </div>
        <!--Danışana Ait raporları detaylı bir biçimde inceleme yapılabilmesi için yapılmıştır -->
        <!--[TODO] Raporların Daha Detaylı Görüntülenebilmesi fancybox sisteme entegre edilmelidir.  -->
        <div class="col-md-12">
            <div class="card card-topline-yellow">
                <div class="card-head">
                    <header><?php echo $item->kullanici_isim; ?> <?php echo $item->kullanici_soyisim; ?> DANIŞANINA AİT RAPORLARI DETAYLI İNCELEME</header>
                    <div class="tools">
                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                        <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                        <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                    </div>
                </div>
                <div class="card-body" id="bar-parent">
                    <ul class="activity-list">
                        <?php if(empty($item_images)) { ?>

                            <div class="alert alert-info text-center">
                                <p>Bu Danışana ait herhangi bir rapor detayı bulunamadı!</a></p>
                            </div>

                        <?php } else { ?>
                        <?php foreach($item_images as $image){ ?>
                        <li>
                            <div class="avatar">
                                <?php if(strcmp($item->cinsiyet,'Erkek') == 0) {?>
                                    <img src="<?php echo base_url("assets"); ?>/img/doc/male.png" alt="">
                                <?php } else{ ?>
                                    <img src="<?php echo base_url("assets"); ?>/img/doc/female.png" alt="">
                                <?php } ?>
                            </div>
                            <div class="activity-desk">
                                <h5><a href="#"><?php echo $item->kullanici_isim; ?> <?php echo $item->kullanici_soyisim; ?></a></h5>
                                <p class="text-muted">Rapor Yüklenme Tarihi : <b><?php echo $image->created_date; ?></b></p>
                                <div align="center">
                                    <a href="#">
                                       <img src="<?php echo base_url("uploads/{$viewFolder}/{$image->img_url}"); ?>" align="center" width="500" height="700" class="img-responsive">
                                    </a>
                                </div>
                            </div>
                        </li>
                        <?php } ?>
                    </ul>
                    <?php } ?>
                </div>
            </div><!-- .widget-body -->




            <!-- start widget -->
            <!-- end widget -->
            <!-- chart start -->
            <!-- Chart end -->
            <!-- start admited patient list -->
            <!-- end admited patient list -->
        </div>

    </div>
