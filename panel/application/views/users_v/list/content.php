<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Danışanları Listeleme</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Anasayfa</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Danışanları Listeleme</li>
                </ol>
            </div>
        </div>

        <a  class="btn blue btn-outline btn-lg m-b-10" href="<?php echo base_url("users/new_form"); ?>"><i class="fa fa-user-plus"></i> DANIŞAN Ekle</a>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-topline-aqua">
                    <div class="card-head">
                        <header>Danışanlar İNCELE</header>
                        <div class="tools">
                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="card-body ">

                        <?php if(empty($items)) { ?>
                            <div class="alert alert-info text-center">
                                <p>Bu ürüne ait resim bulunamadı.Eklemek İçin lütfen <a href="<?php echo base_url("product/new_form"); ?>"></a> tıklayınız.</p>
                            </div>
                        <?php } else {?>

                        <table id="example1" class="display content-container" style="width:100%;">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Danışan Ad</th>
                                <th>Danışan Soyad</th>
                                <th>Cinsiyet</th>
                                <th>Telefon</th>
                                <th>Durumu</th>
                                <th>İşlem</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($items as $item) { ?>
                                <tr>
                                    <?php if(strcmp($item->cinsiyet,'Erkek') == 0) {?>
                                    <td class="patient-img" align="center"><img src="<?php echo base_url("assets"); ?>/img/doc/male.png" alt=""></td>
                                    <?php } else{ ?>
                                    <td class="patient-img" align="center"><img src="<?php echo base_url("assets"); ?>/img/doc/female.png" alt=""></td>
                                    <?php }?>

                                    <td><?php echo $item ->kullanici_isim; ?></td>
                                    <td><?php echo $item ->kullanici_soyisim; ?></td>
                                    <td><?php echo $item ->cinsiyet; ?></td>
                                    <td><?php echo $item ->telephone; ?></td>
                                    <td>
                                        <label class="switchToggle">
                                            <input
                                                    data-url="<?php echo base_url("users/isActiveSetter/{$item ->id}"); ?>"
                                                    class="isActive" type="checkbox" <?php echo ($item ->isActive) ? "checked": ""; ?>>
                                            <span class="slider aqua round"></span>
                                        </label>
                                    </td>
                                    <td>
                                        <a class="btn purple btn-outline m-b-10" href="<?php echo base_url("users/profile/{$item ->id}"); ?>">
                                            <i class="fa fa-user-circle"></i>PROFİL
                                        </a>
                                        <a class="btn blue-bgcolor btn-outline m-b-10" href="<?php echo base_url("users/update_form/{$item ->id}"); ?>">
                                            <i class="fa fa-pencil"></i>Düzenle
                                        </a>
                                        <button  class="btn red btn-outline m-b-10 remove-btn" data-url="<?php echo base_url("users/delete/{$item ->id}"); ?>">
                                            <i class="fa fa-trash-o "></i>SİL
                                        </button>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>

                        </table>

                        <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- start widget -->
        <!-- end widget -->
        <!-- chart start -->
        <!-- Chart end -->
        <!-- start admited patient list -->
        <!-- end admited patient list -->
    </div>

