<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Ürün Fotoğrafları</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Anasayfa</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Ürün Fotoğrafları</li>
                </ol>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card card-topline-purple">
                <div class="card-head">
                    <header><?php echo $item->title; ?> ürününe RESİM ekleme</header>
                    <div class="tools">
                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                        <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                        <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                    </div>
                </div>
                <div class="card-body" id="bar-parent">
                    <form data-url="<?php echo base_url("product/refresh_image_list/{$item->id}"); ?>" id="dropzone" class="dropzone" action="<?php echo base_url("product/image_upload/{$item->id}"); ?>"   data-options="{ url: '<?php echo base_url("product/image_upload/{$item->id}"); ?>'}"></form>
                </div>
            </div>
            <div class="row">

            </div>

        </div>
        <div class="col-md-12">
            <div class="card card-topline-aqua">
                <div class="card-head">
                    <header><?php echo $item->title; ?> ürününe aİt RESİMLER</header>
                    <div class="tools">
                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                        <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                        <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                    </div>
                </div>
                <div class="card-body" id="bar-parent">
                        <?php $this->load->view("{$viewFolder}/{$subViewFolder}/render_elements/image_list_v"); ?>
                </div>
                    </div><!-- .widget-body -->
                </div><!-- .widget -->

            </div>
    </div>




            <!-- start widget -->
        <!-- end widget -->
        <!-- chart start -->
        <!-- Chart end -->
        <!-- start admited patient list -->
        <!-- end admited patient list -->
    </div>
</div>
