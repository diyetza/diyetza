<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Haber Düzenle</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Anasayfa</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Haber Düzenleme</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="card card-box">
                    <div class="card-head">
                        <header>Haber Düzenleme Formu</header>
                    </div>
                    <div class="card-body " id="bar-parent">
                        <form action="<?php echo base_url("references/update/{$item->id}"); ?>" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="simpleFormEmail">Referans Başlık</label>
                                <input type="text-area" class="form-control" name="title" value="<?php echo $item->title; ?>">
                                <?php if(isset($form_error)) { ?>
                                    <small class=" input-form-error"><?php echo form_error("title"); ?></small>
                                <?php } ?>
                            </div>
                            <div class="form-group">
                                <label>Referans Açıklama</label>
                                <textarea class="form-control" rows="4" name="description" ><?php echo $item->description; ?></textarea>
                            </div>

                            <div class="form-group">
                                    <label>Görünen Resim</label>
                                    <div class=" image_upload_container">
                                        <img src="<?php echo base_url("uploads/$viewFolder/$item->img_url"); ?>" alt="" width="150">
                                    </div>
                                    <div
                                    <div class=" form-group image_upload_container" ">
                                        <label>Güncellemek İçin Resim Seçiniz</label>
                                        <input type="file" name="img_url" class="form-control">
                                    </div>

                                </div>




                            <div class="form-group">
                            </div>

                            <button type="submit" class="btn btn-primary">Güncelle</button>
                            <a onclick="window.location.href='<?php echo base_url("news"); ?>'" class="btn btn-danger">Vazgeç</a>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- start widget -->
        <!-- end widget -->
        <!-- chart start -->
        <!-- Chart end -->
        <!-- start admited patient list -->
        <!-- end admited patient list -->
    </div>
</div>
