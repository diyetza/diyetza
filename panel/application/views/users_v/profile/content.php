<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Danışan Profili</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="<?php echo base_url(); ?>">Anasayfa</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Danışan Profii</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PROFILE SIDEBAR -->
                <div class="profile-sidebar">
                    <div class="card card-topline-aqua">
                        <div class="card-body no-padding height-9">
                            <div class="row">
                                <div class="profile-userpic">
                                    <?php if(strcmp($item->cinsiyet,'Erkek') == 0) {?>
                                    <img src="<?php echo base_url("assets"); ?>/img/doc/male.png" alt="">
                                    <?php } else{ ?>
                                    <img src="<?php echo base_url("assets"); ?>/img/doc/female.png" alt="">
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="profile-usertitle">
                                <div class="profile-usertitle-name"><?php echo $item->kullanici_isim; ?> <?php echo $item->kullanici_soyisim; ?></div>
                                <div class="profile-usertitle-job"> <?php echo $item->cinsiyet; ?> </div>
                            </div>
                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item">
                                    <b>Telefon Numarası</b> <a class="pull-right"><?php echo $item->telephone; ?></a>
                                </li>
                                <li class="list-group-item">
                                    <b>E-Posta Adresi</b> <a class="pull-right"><?php echo $item->email; ?></a>
                                </li>
                                <li class="list-group-item">
                                    <b>Diyet Paketi</b> <a class="pull-right"><?php echo $item->kullanici_paket; ?></a>
                                </li>
                            </ul>
                            <!-- END SIDEBAR USER TITLE -->
                            <!-- SIDEBAR BUTTONS -->
                            <div class="profile-userbuttons">
                                <a type="button" class="btn btn-circle deepPink-bgcolor btn-sm" href="<?php echo base_url("users/image_form/{$item ->id}"); ?>">Sağlık Raporları</a>
                                <button type="button" class="btn btn-circle blue btn-sm">BMİ SONUÇLARI</button>
                            </div>
                            <!-- END SIDEBAR BUTTONS -->
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-head card-topline-aqua">
                            <header>Danışan Adres</header>
                        </div>
                        <div class="card-body no-padding height-9">
                            <div class="row text-center m-t-10">
                                <div class="col-md-12">
                                    <p><?php echo $item->adres; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-head card-topline-aqua">
                            <header>Kan Grubu</header>
                        </div>
                        <div class="card-body no-padding height-9">
                            <div class="row text-center m-t-10">
                                <div class="col-md-12">
                                    <p><?php echo $item->kan_grubu; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- END BEGIN PROFILE SIDEBAR -->
                <!-- BEGIN PROFILE CONTENT -->
                <div class="profile-content">
                    <div class="row">
                        <div class="card">
                            <div class="card-topline-aqua">
                                <header></header>
                            </div>
                            <div class="white-box">
                                <!-- Nav tabs -->
                                <div class="p-rl-20">
                                    <ul class="nav customtab nav-tabs" role="tablist">
                                        <li class="nav-item"><a href="#tab1" class="nav-link active" data-toggle="tab">Danışan Notları</a></li>
                                        <li class="nav-item"><a href="#tab2" class="nav-link" data-toggle="tab">Activity</a></li>
                                    </ul>
                                </div>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane active fontawesome-demo" id="tab1">
                                        <div id="biography">
                                            <div class="row">
                                            <h4 class="font-bold"> Kişisel Bilgileri</h4>
                                            </div>
                                            <hr>
                                            <p class="m-t-30">Completed my graduation in Gynaecologist Medicine from the well known and renowned
                                                institution of India – SARDAR PATEL MEDICAL COLLEGE, BARODA in 2000-01, which was affiliated to M.S.
                                                University. I ranker in University exams from the same university from 1996-01.</p>
                                            <p>Worked as Professor and Head of the department ; Community medicine Department at Sterline Hospital,
                                                Rajkot, Gujarat from 2003-2015 </p>
                                            <p>And I was lucky to train in a collegial environment where we called most of our attendings by their
                                                first names. If only doctors did it that way outside the Midwest. One of my attendings even made the
                                                argument that it is safer for patient care because it’s easier for subordinates to raise concerns when
                                                they’re not verbally kowtowing to their superior. I never respected a white-haired surgeon any less when
                                                I addressed him by his first name. In fact, I saw that in non-clinical science, it is commonplace for
                                                the most junior researchers to call the most celebrated senior scientists by their first names.</p>
                                            <p>When I offer or recommend products, I do so because I have actively researched them and find they are
                                                the best in that category for your health. I ignore substandard products, and products not directly
                                                pertinent to your health, regardless of any potential financial upsid</p>
                                            <br>
                                            <h4 class="font-bold">Sağlık Sorunları</h4>
                                            <hr>
                                            <ul>
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="checkbox checkbox-aqua">
                                                            <input id="checkboxbg4" type="checkbox" checked="checked">
                                                            <label for="checkboxbg4">
                                                                Akdeniz Anemisi
                                                            </label>
                                                        </div>
                                                        <div class="checkbox checkbox-aqua">
                                                            <input id="checkboxbg4" type="checkbox" checked="checked">
                                                            <label for="checkboxbg4">
                                                                Aqua Checkbox
                                                            </label>
                                                        </div>
                                                        <div class="checkbox checkbox-aqua">
                                                            <input id="checkboxbg4" type="checkbox" checked="checked">
                                                            <label for="checkboxbg4">
                                                                Aqua Checkbox
                                                            </label>
                                                        </div>
                                                        <div class="checkbox checkbox-aqua">
                                                            <input id="checkboxbg4" type="checkbox" checked="checked">
                                                            <label for="checkboxbg4">
                                                                Aqua Checkbox
                                                            </label>
                                                        </div>
                                                    <!-- Hastalıkların Ayrımı -->
                                                        <div class="checkbox checkbox-aqua">
                                                            <input id="checkboxbg4" type="checkbox" checked="checked">
                                                            <label for="checkboxbg4">
                                                                Aqua Checkbox
                                                            </label>
                                                        </div>
                                                        <div class="checkbox checkbox-aqua">
                                                            <input id="checkboxbg4" type="checkbox" checked="checked">
                                                            <label for="checkboxbg4">
                                                                Aqua Checkbox
                                                            </label>
                                                        </div>
                                                        <div class="checkbox checkbox-aqua">
                                                            <input id="checkboxbg4" type="checkbox" checked="checked">
                                                            <label for="checkboxbg4">
                                                                Aqua Checkbox
                                                            </label>
                                                        </div>
                                                        <div class="checkbox checkbox-aqua">
                                                            <input id="checkboxbg4" type="checkbox" checked="checked">
                                                            <label for="checkboxbg4">
                                                                Aqua Checkbox
                                                            </label>
                                                        </div>
                                                </div>
                                            </ul>
                                            <br>
                                            <h4 class="font-bold">Experience</h4>
                                            <hr>
                                            <ul>
                                                <li>One year rotatory internship from April-2009 to march-2010 at B. J. Medical College, Ahmedabad.</li>
                                                <li>Three year residency at V.S. General Hospital as a resident in orthopedics from April - 2008 to
                                                    April - 2011.</li>
                                                <li>I have worked as a part time physiotherapist in Apang manav mandal from 1st june 2004 to 31st jan
                                                    2005.</li>
                                                <li>Clinical and Research fellowship in Scoliosis at Shaurashtra University and Medical Centre (KUMC) ,
                                                    Krishna Hospital , Rajkot from April 2013 to June 2013.</li>
                                                <li>2.5 Years Worked at Mahatma Gandhi General Hospital, Surendranagar.</li>
                                                <li>Consultant Orthopedics Surgeon Jalna 2 years.</li>
                                            </ul>
                                            <br>
                                            <h4 class="font-bold">Conferences, Cources & Workshop Attended</h4>
                                            <hr>
                                            <ul>
                                                <li>3 days conference of Indian Orthopedics Association (IOACON) held in December 2001 in Ahmedabad,
                                                    Gujarat.</li>
                                                <li>4th Annual Karnataka state physiotherapy students conference</li>
                                                <li>3 day conference of Association of Spine Surgeons of India(ASSICON) held in Jan 2007 in Ahmedabad,
                                                    India.</li>
                                                <li>16th Annual Conference GOACON ' 98 Ahmedabad CME 6th February, 1998</li>
                                                <li>5 day conference of Indian Orthopedic Association (IOACON) held in Dec.2005 in Mumbai, India.</li>
                                                <li>Indian Medical Association Kapadwanj C.M.E. on 22nd April, 2004.</li>
                                                <li>Post Graduate lecture course for Gujarat Orthopaedic Association Ahmedabad 6th to 7th August, 2005.</li>
                                            </ul>
                                            <br>
                                            <h4 class="font-bold">Professional Affiliations </h4>
                                            <hr>
                                            <ul>
                                                <li>Life member: Association of Spine Surgeons’ of India.</li>
                                                <li>Life member: Gujarat Orthopaedic Association.</li>
                                                <li>Life Member: The Indian Society for Bone and Mineral Research (ISBMR).</li>
                                                <li>Life member: Ahmedabad Orthopaedic Society</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab2">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="full-width p-rl-20">
                                                    <div class="panel">
                                                        <form>
                                                            <textarea class="form-control p-text-area" rows="4" placeholder="Whats in your mind today?"></textarea>
                                                        </form>
                                                        <footer class="panel-footer">
                                                            <button class="btn btn-post pull-right">Post</button>
                                                            <ul class="nav nav-pills p-option">
                                                                <li>
                                                                    <a href="#"><i class="fa fa-user"></i></a>
                                                                </li>
                                                                <li>
                                                                    <a href="#"><i class="fa fa-camera"></i></a>
                                                                </li>
                                                                <li>
                                                                    <a href="#"><i class="fa  fa-location-arrow"></i></a>
                                                                </li>
                                                                <li>
                                                                    <a href="#"><i class="fa fa-meh-o"></i></a>
                                                                </li>
                                                            </ul>
                                                        </footer>
                                                    </div>
                                                </div>
                                                <div class="full-width p-rl-20">
                                                    <ul class="activity-list">
                                                        <li>
                                                            <div class="avatar">
                                                                <img src="img/user/user1.jpg" alt="" />
                                                            </div>
                                                            <div class="activity-desk">
                                                                <h5><a href="#">Rajesh</a> <span>Uploaded 3 new photos</span></h5>
                                                                <p class="text-muted">7 minutes ago near Alaska, USA</p>
                                                                <div class="album">
                                                                    <a href="#">
                                                                        <img alt="" src="img/mega-img1.jpg">
                                                                    </a>
                                                                    <a href="#">
                                                                        <img alt="" src="img/mega-img2.jpg">
                                                                    </a>
                                                                    <a href="#">
                                                                        <img alt="" src="img/mega-img3.jpg">
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="avatar">
                                                                <img src="img/user/user3.jpg" alt="" />
                                                            </div>
                                                            <div class="activity-desk">
                                                                <h5><a href="#">John Doe</a> <span>attended a meeting with</span>
                                                                    <a href="#">Lina Smith.</a></h5>
                                                                <p class="text-muted">2 days ago near Alaska, USA</p>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="avatar">
                                                                <img src="img/user/user4.jpg" alt="" />
                                                            </div>
                                                            <div class="activity-desk">
                                                                <h5><a href="#">Kehn Anderson</a> <span>completed the task “wireframe design” within the dead line</span></h5>
                                                                <p class="text-muted">4 days ago near Alaska, USA</p>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="avatar">
                                                                <img src="img/user/user5.jpg" alt="" />
                                                            </div>
                                                            <div class="activity-desk">
                                                                <h5><a href="#">Jacob Ryan</a> <span>was absent office due to sickness</span></h5>
                                                                <p class="text-muted">4 days ago near Alaska, USA</p>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PROFILE CONTENT -->
            </div>
        </div>
    </div>
    <!-- end page content -->
    <!-- start chat sidebar -->
    <div class="chat-sidebar-container" data-close-on-body-click="false">
        <div class="chat-sidebar">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon" data-toggle="tab"> <i class="material-icons">chat</i>Chat
                        <span class="badge badge-danger">4</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#quick_sidebar_tab_3" class="nav-link tab-icon" data-toggle="tab"> <i class="material-icons">settings</i>
                        Settings
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <!-- Start Doctor Chat -->
                <div class="tab-pane active chat-sidebar-chat in active show" role="tabpanel" id="quick_sidebar_tab_1">
                    <div class="chat-sidebar-list">
                        <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd" data-wrapper-class="chat-sidebar-list">
                            <div class="chat-header">
                                <h5 class="list-heading">Online</h5>
                            </div>
                            <ul class="media-list list-items">
                                <li class="media"><img class="media-object" src="img/doc/doc3.jpg" width="35" height="35" alt="...">
                                    <i class="online dot"></i>
                                    <div class="media-body">
                                        <h5 class="media-heading">John Deo</h5>
                                        <div class="media-heading-sub">Spine Surgeon</div>
                                    </div>
                                </li>
                                <li class="media">
                                    <div class="media-status">
                                        <span class="badge badge-success">5</span>
                                    </div> <img class="media-object" src="img/doc/doc1.jpg" width="35" height="35" alt="...">
                                    <i class="busy dot"></i>
                                    <div class="media-body">
                                        <h5 class="media-heading">Rajesh</h5>
                                        <div class="media-heading-sub">Director</div>
                                    </div>
                                </li>
                                <li class="media"><img class="media-object" src="img/doc/doc5.jpg" width="35" height="35" alt="...">
                                    <i class="away dot"></i>
                                    <div class="media-body">
                                        <h5 class="media-heading">Jacob Ryan</h5>
                                        <div class="media-heading-sub">Ortho Surgeon</div>
                                    </div>
                                </li>
                                <li class="media">
                                    <div class="media-status">
                                        <span class="badge badge-danger">8</span>
                                    </div> <img class="media-object" src="img/doc/doc4.jpg" width="35" height="35" alt="...">
                                    <i class="online dot"></i>
                                    <div class="media-body">
                                        <h5 class="media-heading">Kehn Anderson</h5>
                                        <div class="media-heading-sub">CEO</div>
                                    </div>
                                </li>
                                <li class="media"><img class="media-object" src="img/doc/doc2.jpg" width="35" height="35" alt="...">
                                    <i class="busy dot"></i>
                                    <div class="media-body">
                                        <h5 class="media-heading">Sarah Smith</h5>
                                        <div class="media-heading-sub">Anaesthetics</div>
                                    </div>
                                </li>
                                <li class="media"><img class="media-object" src="img/doc/doc7.jpg" width="35" height="35" alt="...">
                                    <i class="online dot"></i>
                                    <div class="media-body">
                                        <h5 class="media-heading">Vlad Cardella</h5>
                                        <div class="media-heading-sub">Cardiologist</div>
                                    </div>
                                </li>
                            </ul>
                            <div class="chat-header">
                                <h5 class="list-heading">Offline</h5>
                            </div>
                            <ul class="media-list list-items">
                                <li class="media">
                                    <div class="media-status">
                                        <span class="badge badge-warning">4</span>
                                    </div> <img class="media-object" src="img/doc/doc6.jpg" width="35" height="35" alt="...">
                                    <i class="offline dot"></i>
                                    <div class="media-body">
                                        <h5 class="media-heading">Jennifer Maklen</h5>
                                        <div class="media-heading-sub">Nurse</div>
                                        <div class="media-heading-small">Last seen 01:20 AM</div>
                                    </div>
                                </li>
                                <li class="media"><img class="media-object" src="img/doc/doc8.jpg" width="35" height="35" alt="...">
                                    <i class="offline dot"></i>
                                    <div class="media-body">
                                        <h5 class="media-heading">Lina Smith</h5>
                                        <div class="media-heading-sub">Ortho Surgeon</div>
                                        <div class="media-heading-small">Last seen 11:14 PM</div>
                                    </div>
                                </li>
                                <li class="media">
                                    <div class="media-status">
                                        <span class="badge badge-success">9</span>
                                    </div> <img class="media-object" src="img/doc/doc9.jpg" width="35" height="35" alt="...">
                                    <i class="offline dot"></i>
                                    <div class="media-body">
                                        <h5 class="media-heading">Jeff Adam</h5>
                                        <div class="media-heading-sub">Compounder</div>
                                        <div class="media-heading-small">Last seen 3:31 PM</div>
                                    </div>
                                </li>
                                <li class="media"><img class="media-object" src="img/doc/doc10.jpg" width="35" height="35" alt="...">
                                    <i class="offline dot"></i>
                                    <div class="media-body">
                                        <h5 class="media-heading">Anjelina Cardella</h5>
                                        <div class="media-heading-sub">Physiotherapist</div>
                                        <div class="media-heading-small">Last seen 7:45 PM</div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
                <!-- End Doctor Chat -->
                <!-- Start Setting Panel -->
                <div class="tab-pane chat-sidebar-settings" role="tabpanel" id="quick_sidebar_tab_3">
                    <div class="chat-sidebar-settings-list slimscroll-style">
                        <div class="chat-header">
                            <h5 class="list-heading">Layout Settings</h5>
                        </div>
                        <div class="chatpane inner-content ">
                            <div class="settings-list">
                                <div class="setting-item">
                                    <div class="setting-text">Sidebar Position</div>
                                    <div class="setting-set">
                                        <select class="sidebar-pos-option form-control input-inline input-sm input-small ">
                                            <option value="left" selected="selected">Left</option>
                                            <option value="right">Right</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="setting-item">
                                    <div class="setting-text">Header</div>
                                    <div class="setting-set">
                                        <select class="page-header-option form-control input-inline input-sm input-small ">
                                            <option value="fixed" selected="selected">Fixed</option>
                                            <option value="default">Default</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="setting-item">
                                    <div class="setting-text">Footer</div>
                                    <div class="setting-set">
                                        <select class="page-footer-option form-control input-inline input-sm input-small ">
                                            <option value="fixed">Fixed</option>
                                            <option value="default" selected="selected">Default</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="chat-header">
                                <h5 class="list-heading">Account Settings</h5>
                            </div>
                            <div class="settings-list">
                                <div class="setting-item">
                                    <div class="setting-text">Notifications</div>
                                    <div class="setting-set">
                                        <div class="switch">
                                            <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-1">
                                                <input type="checkbox" id="switch-1" class="mdl-switch__input" checked>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="setting-item">
                                    <div class="setting-text">Show Online</div>
                                    <div class="setting-set">
                                        <div class="switch">
                                            <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-7">
                                                <input type="checkbox" id="switch-7" class="mdl-switch__input" checked>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="setting-item">
                                    <div class="setting-text">Status</div>
                                    <div class="setting-set">
                                        <div class="switch">
                                            <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-2">
                                                <input type="checkbox" id="switch-2" class="mdl-switch__input" checked>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="setting-item">
                                    <div class="setting-text">2 Steps Verification</div>
                                    <div class="setting-set">
                                        <div class="switch">
                                            <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-3">
                                                <input type="checkbox" id="switch-3" class="mdl-switch__input" checked>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="chat-header">
                                <h5 class="list-heading">General Settings</h5>
                            </div>
                            <div class="settings-list">
                                <div class="setting-item">
                                    <div class="setting-text">Location</div>
                                    <div class="setting-set">
                                        <div class="switch">
                                            <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-4">
                                                <input type="checkbox" id="switch-4" class="mdl-switch__input" checked>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="setting-item">
                                    <div class="setting-text">Save Histry</div>
                                    <div class="setting-set">
                                        <div class="switch">
                                            <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-5">
                                                <input type="checkbox" id="switch-5" class="mdl-switch__input" checked>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="setting-item">
                                    <div class="setting-text">Auto Updates</div>
                                    <div class="setting-set">
                                        <div class="switch">
                                            <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-6">
                                                <input type="checkbox" id="switch-6" class="mdl-switch__input" checked>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end chat sidebar -->
</div>
