<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->

<?php $this -> load -> view("includes/head"); ?>
<!-- END HEAD -->

<body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white white-sidebar-color logo-white">
<div class="page-wrapper">
    <!-- start header -->
    <?php $this -> load -> view("includes/header");  ?>
    <!-- end header -->
    <!-- start color quick setting -->
    <!-- end color quick setting -->
    <!-- start page container -->
    <div class="page-container">
        <!-- start sidebar menu -->
        <?php $this -> load -> view("includes/sidebar"); ?>
        <!-- end sidebar menu -->
        <!-- start page content -->
        <?php $this -> load -> view("{$viewFolder}/content"); ?>
        <!-- end page content -->
        <!-- start chat sidebar -->
        <!-- end chat sidebar -->
    </div>
    <!-- end page container -->
    <?php $this -> load -> view("includes/footer"); ?>
    <!-- start footer -->
    <!-- end footer -->
</div>
<!-- start js include path -->

<?php $this -> load -> view("includes/include_script");  ?>
<!-- end js include path -->
</body>

</html>