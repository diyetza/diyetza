<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Ürün Listeleme</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Anasayfa</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Ürün Listeleme</li>
                </ol>
            </div>
        </div>

        <a  class="btn blue btn-outline btn-lg m-b-10" href="<?php echo base_url("product/new_form"); ?>"><i class="fa fa-plus"></i> Ürün Ekle</a>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-topline-aqua">
                    <div class="card-head">
                        <header>ÜRÜNLERİ İNCELE</header>
                        <div class="tools">
                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="card-body ">

                        <?php if(empty($items)) { ?>
                            <div class="alert alert-info text-center">
                                <p>Bu ürüne ait resim bulunamadı.Eklemek İçin lütfen <a href="<?php echo base_url("product/new_form"); ?>"></a> tıklayınız.</p>
                            </div>
                        <?php } else {?>

                        <table id="example1" class="display content-container" style="width:100%;">
                            <thead>
                            <tr>
                                <th> Id</th>
                                <th>Url</th>
                                <th>Başlık</th>
                                <th>Açıklama</th>
                                <th>Durumu</th>
                                <th>İşlem</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($items as $item) { ?>
                                <tr>
                                    <td><?php echo $item ->id; ?></td>
                                    <td><?php echo $item ->url ?></td>
                                    <td><?php echo $item ->title; ?></td>
                                    <td><?php echo $item ->description; ?></td>
                                    <td>
                                        <label class="switchToggle">
                                            <input
                                                    data-url="<?php echo base_url("product/isActiveSetter/{$item ->id}"); ?>"
                                                    class="isActive" type="checkbox" <?php echo ($item ->isActive) ? "checked": ""; ?>>
                                            <span class="slider aqua round"></span>
                                        </label>
                                    </td>
                                    <td>
                                        <a class="btn blue-bgcolor btn-outline m-b-10" href="<?php echo base_url("product/update_form/{$item ->id}"); ?>">
                                            <i class="fa fa-pencil"></i>Düzenle
                                        </a>

                                        <a class="btn purple btn-outline m-b-10" href="<?php echo base_url("product/image_form/{$item ->id}"); ?>">
                                            <i class="fa fa-image"></i>RESİMLER
                                        </a>
                                        <button  class="btn red btn-outline m-b-10 remove-btn" data-url="<?php echo base_url("product/delete/{$item ->id}"); ?>">
                                            <i class="fa fa-trash-o "></i>SİL
                                        </button>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>

                        </table>

                        <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- start widget -->
        <!-- end widget -->
        <!-- chart start -->
        <!-- Chart end -->
        <!-- start admited patient list -->
        <!-- end admited patient list -->
    </div>

