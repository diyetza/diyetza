<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Ürün Ekleme</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Anasayfa</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Ürün Ekleme</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="card card-box">
                    <div class="card-head">
                        <header>Ürün Ekleme Formu</header>
                    </div>
                    <div class="card-body " id="bar-parent">
                        <form action="<?php echo base_url("product/save"); ?>" method="post">
                            <div class="form-group">
                                <label for="simpleFormEmail">Başlık</label>
                                <input type="text-area" class="form-control" name="title" placeholder="Ürününüzün Başlığını Yazın.">
                                <?php if(isset($form_error)) { ?>
                                    <small class=" input-form-error"><?php echo form_error("title"); ?></small>
                                <?php } ?>
                            </div>
                            <div class="form-group">
                                <label>Açıklama</label>
                                <textarea class="form-control" rows="4" name="description" placeholder="Ürününüze Açıklama Yazın ..."></textarea>
                            </div>
                            <div class="form-group">
                            </div>
                            <button type="submit" class="btn btn-primary">Kaydet</button>
                            <a onclick="window.location.href='<?php echo base_url("product"); ?>'" class="btn btn-danger">Vazgeç</a>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- start widget -->
        <!-- end widget -->
        <!-- chart start -->
        <!-- Chart end -->
        <!-- start admited patient list -->
        <!-- end admited patient list -->
    </div>
</div>
