$(document).ready(function () {

    $(".remove-btn").click(function (e) {

        var $data_url = $(this).data("url");

        swal.fire({
            title: 'Emin misiniz?',
            text: "Bu İşlem Geri Alınamaz!",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText : 'Vazgeç',
            confirmButtonText: 'Evet, Sil!'
        }).then((result) => {
            if (result.value) {
                window.location.href = $data_url;
                Swal.fire(
                    'Silme İşlemi Başarılı!',
                    'Silinen Öğeler Geri Alınamaz.',
                    'success'
                )
            }
        })


    });

    $(".isActive").change(function(){

        var $data = $(this).prop("checked");
        var $data_url = $(this).data("url");

        if(typeof $data !== "undefined" && typeof $data_url !== "undefined"){

            $.post($data_url, { data : $data}, function (response) {

            });

        }

    });

    $(document).ready(function(){

        $(".news_type_select").change(function(){

            if($(this).val() === "image"){

                $(".video_url_container").hide();
                $(".image_upload_container").fadeIn();

            } else if ($(this).val() === "video"){

                $(".image_upload_container").hide();
                $(".video_url_container").fadeIn();

            }

        })

    });

    var uploadSection = Dropzone.forElement("#dropzone");

    uploadSection.on("complete", function(){

        Swal.fire(
            'Tebrikler!',
            'Resim Yükleme İşlemi Tamamlandı!',
            'success'
        )
    });

    $(".content-container, .image_list_container").on('change', '.isActive', function(){

        var $data = $(this).prop("checked");
        var $data_url = $(this).data("url");

        if(typeof $data !== "undefined" && typeof $data_url !== "undefined"){

            $.post($data_url, { data : $data}, function (response) {

            });

        }

    })


});