<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Takvim</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Anasayfa</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Takvim</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-9 col-sm-12">
                        <div class="panel">
                            <header class="panel-heading panel-heading-blue">
                                Takvim</header>
                            <div class="panel-body">
                                <div id="calendar" class="has-toolbar"> </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class="panel">
                            <header class="panel-heading panel-heading-blue">
                                Sürükle-Bırak Etkinlikler </header>
                            <div class="panel-body">
                                <div id="external-events">
                                    <form class="inline-form">
                                        <input type="text" value="" class="form-control" placeholder="Etkinlik Başlık..." id="event_title" />
                                        <br />
                                        <a href="javascript:;" id="event_add" class="btn btn-info"> ETKİNLİK Ekle </a>
                                    </form>
                                    <hr />
                                    <div id="event_box" class="mg-bottom-10"></div>
                                    <label class="rt-chkbox rt-chkbox-single rt-chkbox-outline" for="drop-remove"> Tarihten Sonra Sil
                                        <input type="checkbox" class="group-checkable" id="drop-remove" />
                                        <span></span>
                                    </label>
                                    <hr class="visible-xs" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
