<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Danışan Ekleme</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Anasayfa</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Danışan Ekleme</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="card card-box">
                    <div class="card-head">
                        <header>Danışan Ekleme Formu</header>
                    </div>
                    <div class="card-body " id="bar-parent">
                        <form action="<?php echo base_url("users/save"); ?>" method="post">
                            <div class="form-group">
                                <label for="simpleFormEmail">Ad</label>
                                <input type="text-area" class="form-control" name="kullanici_isim" placeholder="Danışan Ad">
                            </div>
                            <div class="form-group">
                                <label for="simpleFormEmail">Soyad</label>
                                <input type="text-area" class="form-control" name="kullanici_soyisim" placeholder="Danışan Soyad">
                            </div>
                            <div class="form-group">
                                <label for="simpleFormEmail">Cinsiyet</label>
                                    <select class="form-control select2" name="cinsiyet">
                                        <optgroup selected label="Cinsiyet Seçiniz">
                                        <option value="Erkek">Erkek</option>
                                        <option value="Kadın"> Kadın</option>
                                        </optgroup>
                                    </select>
                            </div>
                            <div class="form-group">
                                <label for="simpleFormEmail">Telefon</label>
                                <input type="text-area" class="form-control" name="telephone" placeholder="Telefon">
                            </div>
                            <div class="form-group">
                                <label for="simpleFormEmail">E-Posta</label>
                                <input type="email" class="form-control" name="email" placeholder="E-Posta">
                            </div>
                            <div class="form-group">
                                <label for="simpleFormEmail">Şifre</label>
                                <input type="password" class="form-control" name="password" placeholder="Şifre">
                            </div>
                            <div class="form-group">
                                <label>Adres</label>
                                <textarea class="form-control" rows="4" name="adres" placeholder="Danışana ait adres bilgisi ..."></textarea>
                            </div>

                            <div class="form-group">
                                <label for="simpleFormEmail">Danışan Diyet Paket Türü</label>
                                <select class="form-control select2" name="kullanici_paket">
                                    <optgroup selected label="Online Diyet Paketleri">
                                        <option value="1 Aylık">1 Aylık</option>
                                        <option value="3 Aylık">3 Aylık</option>
                                        <option value="6 Aylık">6 Aylık</option>
                                        <option value="12 Aylık">12 Aylık</option>
                                    </optgroup>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="simpleFormEmail">Kan Grubu</label>
                                <select class="form-control select2" name="kan_grubu">
                                    <optgroup selected label="A Grubu">
                                        <option value="A Rh +">A Rh +</option>
                                        <option value="A Rh -">A Rh -</option>
                                    </optgroup>
                                    <optgroup selected label="B Grubu">
                                        <option value="B Rh +">B Rh +</option>
                                        <option value="B Rh -">B Rh -</option>
                                    </optgroup>
                                    <optgroup selected label="AB Grubu">
                                        <option value="AB Rh +">AB Rh +</option>
                                        <option value="AB Rh -">AB Rh -</option>
                                    </optgroup>
                                    <optgroup selected label="0 Grubu">
                                        <option value="0 Rh +">0 Rh +</option>
                                        <option value="0 Rh -">0 Rh -</option>
                                    </optgroup>
                                </select>
                            </div>
                            <div class="form-group">
                            </div>
                            <a onclick="window.location.href='<?php echo base_url("users"); ?>'" class="btn btn-danger pull-right">Vazgeç</a>
                            &emsp;
                            <button type="submit" class="btn btn-primary pull-right">Kaydet</button>
                            &emsp;




                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- start widget -->
        <!-- end widget -->
        <!-- chart start -->
        <!-- Chart end -->
        <!-- start admited patient list -->
        <!-- end admited patient list -->
    </div>
</div>
