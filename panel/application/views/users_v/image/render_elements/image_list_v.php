<?php if(empty($item_images)) { ?>

    <div class="alert alert-info text-center">
        <p>Bu Danışana Ait Rapor Kaydı Bulunamadı!</a></p>
    </div>

<?php } else { ?>

    <table id="example1" class="display image_list_container" style="width:100%;">
        <thead>
        <th class="text-center">Rapor Id</th>
        <th class="text-center">Ekran Görüntüsü</th>
        <th class="text-center">Rapor Yüklenme Tarihi</th>
        <th class="text-center">Durumu</th>
        <th class="text-center">İşlem</th>
        </thead>
        <tbody>

        <?php foreach($item_images as $image){ ?>

            <tr>
                <td class="w100 text-center"><?php echo $image->id; ?></td>
                <td align="center">
                    <img width="30" src="<?php echo base_url("uploads/{$viewFolder}/{$image->img_url}"); ?>" alt="<?php echo $image->img_url; ?>" class="img-responsive" align="center">
                </td>
                <td class="w100 text-center"><?php echo $image->created_date; ?></td>
                <td class="w100 text-center">
                    <label class="switchToggle">
                        <input
                                data-url="<?php echo base_url("users/imageIsActiveSetter/{$image->id}"); ?>"
                                class="isActive" type="checkbox" <?php echo ($image ->isActive) ? "checked": ""; ?>>
                        <span class="slider aqua round"></span>
                    </label>
                </td>
                <td class="w100 text-center">
                    <button  class="btn red btn-outline m-b-10 remove-btn" data-url="<?php echo base_url("users/imageDelete/{$image ->id}/{$image->user_id}"); ?>">
                        <i class="fa fa-trash-o "></i>SİL
                    </button>
                </td>
            </tr>

        <?php } ?>

        </tbody>

    </table>
<?php } ?>