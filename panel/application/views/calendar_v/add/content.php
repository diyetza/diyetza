<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Haber Ekleme</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="index.html">Anasayfa</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Haber Ekleme</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="card card-box">
                    <div class="card-head">
                        <header>Haber Ekleme Formu</header>
                    </div>
                    <div class="card-body " id="bar-parent">
                        <form action="<?php echo base_url("news/save"); ?>" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="simpleFormEmail">Başlık</label>
                                <input type="text-area" class="form-control" name="title" placeholder="Ürününüzün Başlığını Yazın.">
                                <?php if(isset($form_error)) { ?>
                                    <small class=" input-form-error"><?php echo form_error("title"); ?></small>
                                <?php } ?>
                            </div>
                            <div class="form-group">
                                <label>Açıklama</label>
                                <textarea class="form-control" rows="4" name="description" placeholder="Ürününüze Açıklama Yazın ..."></textarea>
                            </div>
                            <div class="form-group">
                                <label>Haberin Türü</label>
                                <select class="form-control news_type_select" name="news_type" >
                                    <option selected disabled><span>Haber Türleri</span></option>
                                    <option <?php echo (isset($news_type) && $news_type == "image") ? "selected" : ""; ?> value="image">Resim</option>
                                    <option <?php echo (isset($news_type) && $news_type == "video") ? "selected" : ""; ?> value="video">Video</option>
                                </select>
                            </div>
                            <?php if(isset($form_error)){ ?>

                                <div class="form-group image_upload_container" style="display: <?php echo ($news_type == "image") ? "block" : "none"; ?>">
                                    <label>Görsel Seçiniz</label>
                                    <input type="file" name="img_url" class="form-control">
                                </div>

                                <div class="form-group video_url_container" style="display: <?php echo ($news_type == "video") ? "block" : "none"; ?>">
                                    <label>Video URL</label>
                                    <input class="form-control" placeholder="Video bağlantısını buraya yapıştırınız" name="video_url">
                                    <?php if(isset($form_error)){ ?>
                                        <small class="pull-right input-form-error"> <?php echo form_error("video_url"); ?></small>
                                    <?php } ?>
                                </div>


                            <?php } else { ?>

                                <div class="form-group image_upload_container">
                                    <label>Görsel Seçiniz</label>
                                    <input type="file" name="img_url" class="form-control">
                                </div>

                                <div class="form-group video_url_container">
                                    <label>Video URL</label>
                                    <input class="form-control" placeholder="Video bağlantısını buraya yapıştırınız" name="video_url">
                                </div>


                            <?php } ?>
                            <div class="form-group">
                            </div>
                            <button type="submit" class="btn btn-primary">Kaydet</button>
                            <a onclick="window.location.href='<?php echo base_url("news"); ?>'" class="btn btn-danger">Vazgeç</a>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- start widget -->
        <!-- end widget -->
        <!-- chart start -->
        <!-- Chart end -->
        <!-- start admited patient list -->
        <!-- end admited patient list -->
    </div>
</div>
